#-----Statement of Authorship----------------------------------------#
#
#  By submitting this task the signatories below agree that it
#  represents our own work and that we both contributed to it.  We
#  are aware of the University rule that a student must not
#  act in a manner which constitutes academic dishonesty as stated
#  and explained in QUT's Manual of Policies and Procedures,
#  Section C/5.3 "Academic Integrity" and Section E/2.1 "Student
#  Code of Conduct".
#
#  First student's no: n8824088
#  First student's name: Scott J. Schultz
#  Portfolio contribution: 100%
#
#  Second student's no: Solo Submission
#
#  Contribution percentages refer to the whole portfolio, not just this
#  task.  Percentage contributions should sum to 100%.  A 50/50 split is
#  NOT necessarily expected.  The percentages will not affect your marks
#  except in EXTREME cases.
#
#--------------------------------------------------------------------#



#-----Task Description-----------------------------------------------#
#
#  TEXT MINER
#
#  In this task you and your partner will develop a Graphical User
#  Interface for a program that allows the user to "mine" a text file
#  for interesting text patterns using Regular Expressions.  See
#  the instructions accompanying this file for full detail.
#
#--------------------------------------------------------------------#



#-----Some Example Search Terms--------------------------------------#
#
# Here are some interesting search terms to try in the
# Jekyll and Hyde HTML document
#
# Things described as strange: [Ss]trange +[a-zA-Z]+
# People with titles: [DM]r\. +[A-Za-z]+
# Mr. Hyde's first name: [A-Z][a-z]* *Hyde
# Dr. Jekyll's full name: Dr\. +[A-Z][a-z]* +Jekyll
# Questions asked by characters in the book: "[A-Z][a-z ]*\?"
# Exclamations: "[A-Z][a-z ]*!"
# Sentences mentioning blood: [A-Z][A-Za-z,;\- ]*blood[A-Za-z,;\- ]*[\.!\?]
# Emphasised words: <i>([^<]*)</i>
# HTML MarkUp tags used: <([a-zA-Z\-_]+)
#
#--------------------------------------------------------------------#



#-----Students' Solution---------------------------------------------#
#
#  This is where you will create your GUI program.
#

#------------------------------------------------------------------------------#
#                                                                              #
#                                   IMPORTS                                    #
#                                                                              #
#------------------------------------------------------------------------------#

# Import the necessary regular expression function
from re import findall

# Import the Tkinter functions
from Tkinter import *

#------------------------------------------------------------------------------#
#                                                                              #
#                                   GLOBALS                                    #
#                                                                              #
#------------------------------------------------------------------------------#


#------------------------------------------------------------------------------#
#                                                                              #
#                                   CONSTANTS                                  #
#                                                                              #
#------------------------------------------------------------------------------#

window_padding = 5

#------------------------------------------------------------------------------#
#                                                                              #
#                                  EXCEPTIONS                                  #
#                                                                              #
#------------------------------------------------------------------------------#

class MissingRegex(Exception):
	"""
	Does literally nothing amazing, just allows me to have better control
	over my error messages without having a billion if/elifs
	"""
	pass

###### CREATE YOUR Text Miner PROGRAM HERE

def render_window():
	"""
	This function creates the whole interface, sets up bindings where needed,
	defines globals for use later on and handles calls to performing searches.

	This function is pretty much the main function.
	"""

	# Define globals for use in other functions
	global search_result_text_area, filename_entry, regex_entry

	text_miner_window = Tk()

	text_miner_window.title('Text Miner')


	# Build our search results frame
	search_result_frame = Frame(text_miner_window)

	search_result_text_area = Text(
		search_result_frame,
		width = 60,
		height = 10,
	)
	search_result_text_area.pack(
		side=LEFT, fill=Y
	)
	search_result_frame.pack(
		padx = window_padding,
		pady = window_padding
	)

	# Bind a scrollbar to our text area that will forever appear inactive because
	# technically 
	scrollbar = Scrollbar(
		search_result_frame
	)
	search_result_text_area.config(
		yscrollcommand=scrollbar.set
	)
	scrollbar.pack(
		side=RIGHT, fill=Y
	)

	# Build our input frame
	input_frame = Frame(text_miner_window)

	# Start with a label for the filename
	filename_label = Label(
		input_frame,
		text = 'File Name:',
		font = ("Arial",12)
	)
	filename_label.grid(
		row = 2,
		column = 1,
		sticky = E
	)

	# Followed by the text entry field and related container variable for its string
	input_filename = StringVar()
	filename_entry = Entry(
		input_frame,
		justify = LEFT,
		textvariable = input_filename,
		width = 30,
		font = ("Arial",12)
	)
	filename_entry.grid(
		row = 2,
		column = 2,
		sticky = W
	)

	# Give this field input the moment its created
	filename_entry.focus_set()

	# Same deal for the regex row
	regex_label = Label(
		input_frame,
		text = 'Regular Expression:',
		font = ("Arial",12)
	)
	regex_label.grid(
		row = 3,
		column = 1,
		sticky = E,
		pady = window_padding
	)

	input_regex = StringVar()
	regex_entry = Entry(
		input_frame,
		justify = LEFT,
		textvariable = input_regex,
		width = 30,
		font = ("Arial",12)
	)
	regex_entry.grid(
		row = 3,
		column = 2,
		sticky = W
	)

	# And finally our search button
	search_button = Button(
		input_frame,
		text ="Search",
		command = lambda:open_file(input_filename,input_regex)
	)
	search_button.grid(
		row = 4,
		column = 1,
		columnspan = 3
	)

	# Pack it all together in our frame.
	input_frame.pack(
		padx = window_padding,
		pady = window_padding
	)

	# Because using lambda event: makes more sense than just using open_file()
	# by itself. Otherwise the function fires as soon as the script is run instead
	# of on enter keypress.
	# Why? Why not? It's python, it doesn't need to make sense!

	# Then bind the enterkey to perform a search in the silliest way I've seen yet.
	text_miner_window.bind(
		"<Return>",
		lambda event: open_file(input_filename, input_regex)
	)

	# Then start tkinters' looping so that the program responds to input
	text_miner_window.mainloop()

def open_file(filename,regex):
	"""
	Attempts to open a file.

	This function will first attempt to open a given file and read its contents.

	If the file can't be found using a given filename, IOError 2 will be raised.
	If the function isn't given a filename at all, IOError 22 is raised instead.
	In either case the exception stems from IOError.

	All errors set the appropriate fields red on error, and green on success/ok.
	"""
	global filename_entry, search_result_text_area


	try:
		file_contents = open(filename.get(),"U").read()
	except IOError as IOe:
		filename_entry.configure(
			bg = 'red'
		)
		if IOe[0] == 22:
			search_result_text_area.configure(
				bg = 'red'
			)

			# This basically just clears the whole text area, because having a
			# clear() method would be silly.
			search_result_text_area.delete(
				"1.0",
				END
			)
			search_result_text_area.insert(
				INSERT,
				"No filename given."
			)
		if IOe[0] == 2:
			search_result_text_area.configure(
				bg = 'red'
			)
			search_result_text_area.delete(
				"1.0",
				END
			)
			search_result_text_area.insert(
				INSERT,
				"Could not find file: {filename}.".format(
					filename = filename.get()
				)
			)
	except Exception as e:
		filename_entry.configure(
			bg = 'red'
		)
		search_result_text_area.delete(
			"1.0",
			END
		)
		search_result_text_area.insert(
			INSERT,
			"Something went wrong :(.\n {error}.".format(
				error = e
			)
		)
	else:
		filename_entry.configure(
			bg = 'green'
		)
		run_regex(regex,file_contents)

def run_regex(regex,file_contents):
	"""
	Attempts to perform a regex search on a given file string with a given regex string.

	First determains if there was anything entered into the regex field. If nothing
	was passed, throw a MissingRegex exception. Otherwise attempt to run the regex,
	remove duplicates and sort the list into alphabetical order.

	Mostly the same structure as the open_file() function, errors are red,
	success/ok is green
	"""
	global regex_entry, search_result_text_area

	try:
		if not regex.get():
			# I want a slightly more specific way to detect and handle my errors,
			# so I'll use my own exception class.
			raise MissingRegex("No regex was given.")
		else:
			regex_results = sorted(set(findall(regex.get(), file_contents)))
	except MissingRegex, e:
		regex_entry.configure(
			bg = 'red'
		)
		search_result_text_area.configure(
			bg = 'red'
		)
		search_result_text_area.delete(
			"1.0",
			END
		)
		search_result_text_area.insert(
			INSERT,
			"Error: {error}".format(
				error = e
			)
		)
	except Exception, e:
		regex_entry.configure(
			bg = 'red'
		)
		search_result_text_area.configure(
			bg = 'red'
		)
		search_result_text_area.delete(
			"1.0",
			END
		)
		search_result_text_area.insert(
			INSERT,
			"Invalid regex: {regex}.\nException: {error}".format(
				regex = regex.get(),
				error = e
			)
		)
	else:

		# We've got this far without any errors, now lets see if we got any results
		if regex_results:

			# We got 1 or more results, so lets make sure the search results box
			# is a pleasant white and the regex entry is a nice green.
			regex_entry.configure(
				bg = 'green'
			)
			search_result_text_area.configure(
				bg = 'white'
			)
			search_result_text_area.delete(
				"1.0",
				END
			)

			# Set the focus to the search result area to allow for
			# immediate and seemless scrolling. If I didn't tell you, you wouldn't
			# notice. But if this wasn't here the program would seem awkward to use.
			search_result_text_area.focus_set()

			# Loop through all of our results and display them in our text area.
			for match in regex_results:
				search_result_text_area.insert(
					INSERT,
					"{match}\n".format(
						match = match
					)
				)
		else:

			# We didn't find anything, so lets use our favorite shade of red again
			# and display an error
			search_result_text_area.configure(
				bg = 'red'
			)
			search_result_text_area.delete(
				"1.0",
				END
			)
			search_result_text_area.insert(
				INSERT,
				"No matches found using regex: {regex}".format(
					regex = regex.get(),
				)
			)

# After everything is done, we still need to call the function that starts it all.
render_window()

#
#--------------------------------------------------------------------#

